# Salesforce Apex

Vendor: Salesforce
Homepage: https://www.salesforce.com/

Product: Apex
Product Page: https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dev_guide.htm

## Introduction
We classify Salesforce Apex into the ITSM or Service Management domain as Salesforce Apex provides a mechanism for interacting with the Salesforce platform, which aligns with multiple ITSM processes.

## Why Integrate
The Salesforce Apex adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Salesforce Apex. 
With this adapter you have the ability to perform operations with Salesforce Apex on items such as:

- Triggers
- Classes
- Tests

## Additional Product Documentation
[API for Salesforce Apex](https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/call_apex_list.htm)